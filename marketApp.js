/* 
Create a market. In the market, create 3 cash registers and 7 customers, 
each customer has a shopping cart with any amount of products inside from 1 to 10, 
products are different from each other and a customer may have more than 1 of the same product.
The 7 customers will go to the 3 cash registers more or less in an equal split 
and each register should calculate the total amount each customer should pay.
As there is currently a promotion, any product that costs more than 35 will get a 10% discount
 and any shopping cart which total is more than 100 will get an additional 10% discount.
Display the total of the carts with the `$` sign preceding the value.

*/

//Create a Market Object with factory function 
const market = {
    bananas:{
        name:'bananas',
        price:5
    },
    milk:{
        name:'milk',
        price:37
    },
    oranges:{
        name:'oranges',
        price:6
    },
    apples:{
        name:'apples',
        price:4
    },
    soda:{
        name:'soda',
        price:52
    },
    berries:{
        name:'berries',
        price:14
    },
    coconut:{
        name:'coconut',
        price:7
    },
    pasta:{
        name:'pasta',
        price:50
    },
    chesse:{
        name:'cheese',
        price:45
    },
    wine:{
        name:'wine',
        price:67
    },
    beer:{
        name:'beer',
        price:24
    }
}
//Create a Customer Object with factory function
function customer(id){
    return{
        id:id,
        basket:[],
       /*  displayBasketItems: function(){
            for(item in this.basket){
                console.log(this.basket[item])
            }
        }, */
        printTotal:function(){
            let total = 0
            for(item in this.basket){ 
                total += this.basket[item]
            }
            console.log(`Your total for today is: $${total} dollars.`)
        }
    }
}
//Create a Register Object with factory function
function register(id){
    return{
        id:id,
        displayTotal: function(array){
            let total = 0
            let overAllDiscount = 0 
            for(item in array){
              total += array[item]
              //Search items and give discount by using a condition (if else)
              if(array[item] >= 35 ){
                  //Apply 10% discount to any product more than 35
                  let itemDiscount = total * 10 / 100
                  total = total - itemDiscount
              }
              //Apply a 10 % discount if the bill is greater than 100 
              if(total > 100){
                  overAllDiscount = total * 10 / 100
                  total = total - overAllDiscount
              }
            }
           
            console.log(`Your total for today is: $${total.toFixed(2)} dollars. `)
        }
    }
}
//Create 7 Customers
const customer1 = customer(1)
const customer2 = customer(2)
const customer3 = customer(3)
const customer4 = customer(4)
const customer5 = customer(5)
const customer6 = customer(6)
const customer7 = customer(7)

//Create 3 Cash Registers
const register1 = register(1)
const register2 = register(2)
const register3 = register(3)

//Add items to each customer's basket
customer1.basket.push(
    market.soda.price,
    market.beer.price,
    market.coconut.price,
    market.pasta.price,
    market.apples.price,
    market.milk.price,
    market.chesse.price,
    market.berries.price,
    market.wine.price,
    market.oranges.price
    )
    //Cash out customer 1 cart with display in price $
    register1.displayTotal(customer1.basket)

